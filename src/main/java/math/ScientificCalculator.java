package math;

public class ScientificCalculator extends StandardCalculator{
    public double pow(double a, double b){
        //double c=1;for(int i=0; i<a; i++){ c=c*b; }
        return Math.pow(b,a);
    }
    public double root(double a, double b){
        return Math.pow(b,1/a);
    }
    public double nSin(double a){
        return Math.sin(Math.toRadians(a));
    }
    public double nCos(double a){
        return Math.cos(Math.toRadians(a));
    }
    public double nTan(double a){
        return Math.tan(Math.toRadians(a));
    }
}
