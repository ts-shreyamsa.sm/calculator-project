package math;

public class StandardCalculator {

    public double add(double a, double b){
        return b+a;
    }
    public double sub(double a, double b){
        return b-a;
    }
    public double mul(double a, double b){
        return b*a;
    }
    public double div(double a, double b){
        return b/a;
    }
    public int mod(double a, double b){
        return (int) (b%a);
    }
}
