import math.ScientificCalculator;
import math.StandardCalculator;

import java.io.*;
import java.util.Scanner;
import java.util.Stack;

public class Application {

    public static void main(String[] args) throws IOException {
        //System.out.println("Hello");
        Scanner in = new Scanner(System.in);
        System.out.println("1. To open Standard Calculator type std\n2. To open Scientific Calculator type sci\n");
        String mode = in.next();
        //System.out.println(mode);
        char cont= 'y';
        double memory=0;
        while(cont != 'n'){
            System.out.println("Enter an expression: ");
            Scanner input = new Scanner(System.in);
            String expression = input.nextLine();
            //expression = expression.replaceAll(" ", "");
            //System.out.println(expression);
            Stack<Double> valStack = new Stack<>();
            Stack<Character> parStack = new Stack<>();
            char[] tokens = expression.toCharArray();
            //String tokens[] = expression.split(" ");
            int temp = 0;
            char sciOpeChar = 0;
            for(int i = 0; i < tokens.length; i++){
                if(tokens[i] == '(') {
                    temp++;
                    parStack.push(tokens[i]);
                } else if((tokens[i] >= '0' && tokens[i] <= '9')) {
                    StringBuffer number = new StringBuffer();
                    while(i < tokens.length && ((tokens[i] >= '0' && tokens[i] <= '9')))
                        number.append(tokens[i++]);
                    valStack.push(Double.parseDouble(number.toString()));
                } else if(tokens[i] == ')'){
                    while(parStack.peek() != '(') {
                        if(sciOpeChar == 's' || sciOpeChar == 'c' || sciOpeChar == 't')
                            valStack.push(check(mode, parStack.pop(), valStack.pop(), 0));
                        else valStack.push(check(mode, parStack.pop(), valStack.pop(), valStack.pop()));
                    }
                    temp--;
                    parStack.pop();
                } else if (tokens[i] == '+' || tokens[i] == '-' || tokens[i] == '*' || tokens[i] == '/' || tokens[i] == '%' || tokens[i] == '^' || (tokens[i] >= 'a' && tokens[i] <= 'z')) {
                    StringBuffer opStr = new StringBuffer();
                    while(i < tokens.length && ((tokens[i] >= 'a' && tokens[i] <= 'z')))
                        opStr.append(tokens[i++]);
                    if(opStr.toString().equals("root")) tokens[i] = 'r';
                    else if(opStr.toString().equals("sin")){ tokens[i] = 's'; sciOpeChar = 's';}
                    else if(opStr.toString().equals("cos")){ tokens[i] = 'c'; sciOpeChar = 'c';}
                    else if(opStr.toString().equals("tan")){ tokens[i] = 't'; sciOpeChar = 't';}
                    while (!parStack.empty() && hasPrecedence(tokens[i], parStack.peek())) {
                        if (sciOpeChar == 's' || sciOpeChar == 'c' || sciOpeChar == 't'){
                            valStack.push(check(mode, parStack.pop(), valStack.pop(), 0));
                            sciOpeChar=0;
                        }
                        else valStack.push(check(mode, parStack.pop(), valStack.pop(), valStack.pop()));
                    }
                    parStack.push(tokens[i]);
                }
            }
            if(temp==0) {
                while (!parStack.empty()) {
                    if(sciOpeChar == 's' || sciOpeChar == 'c' || sciOpeChar == 't'){
                        valStack.push(check(mode, parStack.pop(), valStack.pop(), 0));
                        sciOpeChar = 0;
                    }
                    else valStack.push(check(mode, parStack.pop(), valStack.pop(), valStack.pop()));
                }
                double result = valStack.pop();
                System.out.println(result);
                System.out.println("“M” : should remember the result of the last valid operation.\n“MR” : should print the contents of the memory in the next line.\n“MC” : should clear the contents of the memory.\n");
                String memInput = in.next();
                switch (memInput) {
                    case "M":
                        memory=result;
                        System.out.println("Result Saved in memory.");
                        break;
                    case "MR":
                        System.out.println(memory);
                        break;
                    case "MC":
                        memory=0;
                        System.out.println("Memory Cleared.");
                        break;
                }

            }else System.out.println("Bracket Not Valid Or Not Matching");
            System.out.println("Do you want to continue [y/n]");
            String contIn = input.next();
            if(contIn.equals("no") || contIn.equals("n")) cont = 'n';
        }

    }

    private static boolean hasPrecedence(char op1, char op2){
        if (op2 == '(' || op2 == ')') return false;
        else if ((op1 == 's' || op1 == 'c' || op1 == 't' ||op1 == 'r' || op1 == '^' ||op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-' || op2 == '%')) return false;
        else if ((op1 == 's' || op1 == 'c' || op1 == 't' || op1 == 'r' || op1 == '^') && (op2 == '*' || op2 == '/')) return false;
        else return true;
    }

    private static double check(String mode, char operator, double firstOperand, double secondOperand) {
        StandardCalculator std=new StandardCalculator();
        ScientificCalculator sci=new ScientificCalculator();
        double result = 0;
        if(mode.equals("std") || mode.equals("Std") || mode.equals("STD")){
            switch (operator){
                case '+':
                    result = std.add(firstOperand,secondOperand);
                    break;
                case '-':
                    result = std.sub(firstOperand,secondOperand);
                    break;
                case '*':
                    result = std.mul(firstOperand,secondOperand);
                    break;
                case '/':
                    result = std.div(firstOperand,secondOperand);
                    break;
                case '%':
                    result = std.mod(firstOperand,secondOperand);
                    break;
                default:
                    throw new IllegalStateException("Invalid Operation: " + operator);
            }
        }
        if(mode.equals("sci")){
            switch (operator){
                case '+':
                    result = std.add(firstOperand,secondOperand);
                    break;
                case '-':
                    result = std.sub(firstOperand,secondOperand);
                    break;
                case '*':
                    result = std.mul(firstOperand,secondOperand);
                    break;
                case '/':
                    result = std.div(firstOperand,secondOperand);
                    break;
                case '%':
                    result = std.mod(firstOperand,secondOperand);
                    break;
                case '^':
                    result = sci.pow(firstOperand,secondOperand);
                    break;
                case 'r':
                    result = sci.root(firstOperand,secondOperand);
                    break;
                case 's':
                    result = sci.nSin(firstOperand);
                    break;
                case 'c':
                    result = sci.nCos(firstOperand);
                    break;
                case 't':
                    result = sci.nTan(firstOperand);
                    break;
                default:
                    throw new IllegalStateException("Invalid Operation: " + operator);
            }
        }
        return result;
    }
}
